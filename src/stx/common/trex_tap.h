#ifndef __TREX_TAP_H__
#define __TREX_TAP_H__

/*
Copyright (c) 2015-2015 Cisco Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/


#include "pal/linux/mbuf.h"
#include "trex_rx_feature_api.h"

class RXTapForward {
public:
    void create(RXFeatureAPI *api);
    void handle_pkt(const rte_mbuf_t *m);

    void work_tick();

private:
    RXFeatureAPI *m_api;
    int m_tap_fd;
};

#endif  // __TREX_TAP_H__
