#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <net/if.h>
#include <linux/if_tun.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <fcntl.h>
#include <arpa/inet.h> 
#include <sys/select.h>
#include <sys/time.h>
#include <errno.h>
#include <stdarg.h>
#include <string>

#include "trex_tap.h"

#define BUF_SIZE 1600

#define LOG_ERR printf
//#define LOG_DBG printf
#define LOG_DBG if (0) printf

void tap_forward(int from, int to) {
    char buf[BUF_SIZE];
    for(;;) {
        int nread = read(from, buf, sizeof(buf));
        if (nread <= 0) {
            return;
        }
        int nwrite = write(to, buf, nread);
        if (nwrite < 0) {
            LOG_DBG("whoops. rc=%d\n", nwrite);
        }
    }
}

void tap_to_port(int tap_id, RXFeatureAPI *port_api) {
    char buf[BUF_SIZE];
    for(;;) {
        int nread = read(tap_id, buf, sizeof(buf));
        if (nread <= 0) {
            return;
        }

        std::string sbuf(buf, nread); // TODO: keep buf mem
        port_api->tx_pkt(sbuf);
    }
}

int tap_alloc(const char *dev) {
    int flags = IFF_TAP | IFF_NO_PI;

    struct ifreq ifr;
    int fd, err;
    const char *clonedev = "/dev/net/tun";

    if( (fd = open(clonedev, O_RDWR)) < 0 ) {
        return fd;
    }

    /* preparation of the struct ifr, of type "struct ifreq" */
    memset(&ifr, 0, sizeof(ifr));

    ifr.ifr_flags = flags;   /* IFF_TUN or IFF_TAP, plus maybe IFF_NO_PI */

    if (*dev) {
        strncpy(ifr.ifr_name, dev, IFNAMSIZ);
    }

    /* try to create the device */
    if( (err = ioctl(fd, TUNSETIFF, (void *) &ifr)) < 0 ) {
        close(fd);
        return err;
    }

    /* make tun socket non blocking */
    int sock_opts = fcntl(fd, F_GETFL, 0 );
    fcntl(fd, F_SETFL, sock_opts | O_NONBLOCK );
    LOG_ERR("Created tap device %s fd=%d\n", dev, fd);

    return fd;
}


void RXTapForward::create(RXFeatureAPI *api) {
    m_api = api;
    char name[256];
    snprintf(name, sizeof(name), "tap_port_%d", (int)api->get_port_id());
    m_tap_fd = tap_alloc(name);
}

// fwd decl from trex_pkt.h
void mbuf_to_buffer(uint8_t *dest, const rte_mbuf_t *m);

void RXTapForward::handle_pkt(const rte_mbuf_t *m) {
    static int idx = 0;
    int data_sz = rte_pktmbuf_data_len(m) + rte_pktmbuf_pkt_len(m);
    LOG_DBG("Incoming packet #%d len %d, on port %d \n", ++idx, data_sz, (int)m_api->get_port_id());

    char buf[BUF_SIZE];
    if (data_sz > BUF_SIZE) {
        LOG_ERR("Buf is too small\n");
        return;
    }

    mbuf_to_buffer((uint8_t *)buf, m);
    int write_res = write(m_tap_fd, buf, data_sz);
    if (write_res < 0) {
        LOG_ERR("whoops. failed to write buf to tap. sz= %d rc=%d\n", data_sz, write_res);
    }
}

void RXTapForward::work_tick() {
    tap_to_port(m_tap_fd, m_api);
}

